package com.example.meditation2.data.api

import com.example.meditation2.data.model.FeelingsResponse
import com.example.meditation2.data.model.LoginResponse
import com.example.meditation2.data.model.QuotesResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiRetrofit {

    @POST("user/login")
    fun authorize(
        @Header("Content-Type")typus: String = "application/json",
        @Body hashik: HashMap<String, String>
    ): Call<LoginResponse>

    @GET("quotes")
    fun quotesGet(): Call<QuotesResponse>

    @GET("feelings")
    fun feelingsGet(): Call<FeelingsResponse>
}