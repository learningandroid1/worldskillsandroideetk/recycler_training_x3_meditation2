package com.example.meditation2.data.model

import android.provider.ContactsContract

data class LoginResponse(
    val id: String,
    val email: String,
    val nickName: String,
    val avatar: String,
    val token: String
)
