package com.example.meditation2.data.model

data class FeelingsResponse(val success: Boolean, val data: ArrayList<FeelingAttr>)

data class FeelingAttr(
    val id: Int,
    val title: String,
    val image: String
)
