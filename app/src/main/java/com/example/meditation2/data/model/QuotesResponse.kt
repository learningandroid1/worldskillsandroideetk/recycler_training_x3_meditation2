package com.example.meditation2.data.model

data class QuotesResponse(val success: Boolean, val data: ArrayList<QuoteAttr>)

data class QuoteAttr(
    val id: Int,
    val title: String,
    val image: String,
    val description: String
)