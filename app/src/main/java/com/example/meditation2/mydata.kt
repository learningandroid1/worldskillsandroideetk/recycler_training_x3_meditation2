package com.example.meditation2

data class feel(val image: Int, val name: String)
class MyFeel{val spisok = arrayListOf(
      feel(R.drawable.ic_relax, "Расслабленным"),
      feel(R.drawable.ic_calm, "Спокойным"),
      feel(R.drawable.ic_focus, "Сосредоточенным"),
      feel(R.drawable.ic_calm, "Спокойным"),
      feel(R.drawable.ic_relax, "Расслабленным"),
      feel(R.drawable.ic_relax, "Расслабленным"),
      feel(R.drawable.ic_relax, "Расслабленным"),
      feel(R.drawable.ic_relax, "Расслабленным"),
      feel(R.drawable.ic_relax, "Расслабленным")
      )}

data class state(val title: String, val descr: String, val image: Int) // а вот кнопочки тут нет, кнопка стабильна.
class MyState{val spisok2 = arrayListOf(
      state("Статья 1", "Описание к статье 1", R.drawable.image_girl2),
      state("Статья 2", "Описание к статье 2", R.drawable.image_zu),
      state("Статья 3", "Описание к статье 3", R.drawable.image_girl2),
      state("Статья 4", "Описание к статье 4", R.drawable.image_zu)
      )}

data class music(val artist: String, val song: String, val poster: Int)
class MyMusic{val spisok3 = arrayListOf(
      music("Atlantida PRJCT", "Сигнальные Огни", R.drawable.music_poster),
      music("Head Splitter", "Horizon", R.drawable.music_poster3),
      music("Moderator", "Tamboo", R.drawable.music_poster2),
      music("Renzo", "Deep Freeze", R.drawable.music_poster4),
      music("Enjoykin", "You Dont", R.drawable.music_poster5)
      )}