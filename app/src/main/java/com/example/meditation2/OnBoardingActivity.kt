package com.example.meditation2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
    }

    fun toRegister(view: android.view.View) {
        val perehod = Intent(this, EndCapRegisterActivity::class.java)
        startActivity(perehod)
    }
    fun toLogin(view: android.view.View) {
        val perehod = Intent(this, LoginActivity::class.java)
        startActivity(perehod)
    }
}