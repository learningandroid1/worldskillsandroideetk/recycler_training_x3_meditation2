package com.example.meditation2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.meditation2.R
import com.example.meditation2.data.model.FeelingAttr
import com.example.meditation2.feel

class AdapterRecHoriz(val hzcontext: Context, val spisok: ArrayList<FeelingAttr>): RecyclerView.Adapter<AdapterRecHoriz.MyVH>() {
    class MyVH(hzitemView: View): RecyclerView.ViewHolder(hzitemView) {
       val picture: ImageView = hzitemView.findViewById(R.id.image_feel)
       val textik: TextView = hzitemView.findViewById(R.id.text_feel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterRecHoriz.MyVH {
        // здесь создается элемент списка. Inflate - это вдувание из hzcontext в наш адаптер.
       val root = LayoutInflater.from(hzcontext).inflate(R.layout.recycler_horiz_adapter, parent, false)
       return MyVH(root)
    }

    override fun onBindViewHolder(holder: AdapterRecHoriz.MyVH, position: Int) {
        // холдер берёт части (Item) из определенных мест коллекции.
        Glide.with(hzcontext).load(spisok[position].image).into(holder.picture)
        holder.textik.setText(spisok[position].title)
    }

    override fun getItemCount(): Int {
        return spisok.size
    }
}