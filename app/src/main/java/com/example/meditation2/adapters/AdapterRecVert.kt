package com.example.meditation2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.meditation2.R
import com.example.meditation2.data.model.QuoteAttr
import com.example.meditation2.state
import java.util.zip.Inflater

class AdapterRecVert (val hzcontext: Context, val spisok: ArrayList<QuoteAttr>): RecyclerView.Adapter<AdapterRecVert.MyVH>() {
    class MyVH(item: View): RecyclerView.ViewHolder(item) {
    val picture: ImageView = item.findViewById(R.id.image_state) // это вот конкретно, куда вставлять. picture будет вместо image_state
    val text_title: TextView = item.findViewById(R.id.title_state)
    val text_descr: TextView = item.findViewById(R.id.description_state)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterRecVert.MyVH {
        val shtuka = LayoutInflater.from(hzcontext).inflate(R.layout.recycler_vert_adapter, parent, false)
        return MyVH(shtuka)
    }

    override fun onBindViewHolder(holder: AdapterRecVert.MyVH, position: Int) {
        Glide.with(hzcontext).load(spisok[position].image).into(holder.picture) // мы по сути связываем наши три кусочка объекта с элементами из нашей "БД", из лок хранилища.
        holder.text_title.setText(spisok[position].title)
        holder.text_descr.setText(spisok[position].description)
    }

    override fun getItemCount(): Int {
       return spisok.size
    }
}
