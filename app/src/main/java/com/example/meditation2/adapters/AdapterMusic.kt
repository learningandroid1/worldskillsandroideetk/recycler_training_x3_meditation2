package com.example.meditation2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.meditation2.MyMusic
import com.example.meditation2.R
import com.example.meditation2.music

class AdapterMusic(val blyacontext: Context, val massive: ArrayList<music>): RecyclerView.Adapter<MusicViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicViewHolder {
       val root = LayoutInflater.from(blyacontext).inflate(R.layout.music_adapter, parent, false)
        return MusicViewHolder(root)
    }

    override fun onBindViewHolder(holder: MusicViewHolder, position: Int) {
       holder.artistname.text = massive[position].artist
       holder.songname.text = massive[position].song
       holder.poster.setImageResource(massive[position].poster)
    }

    override fun getItemCount(): Int {
      return massive.size
    }
}

class MusicViewHolder(item1: View): RecyclerView.ViewHolder(item1) {
    val artistname: TextView = item1.findViewById(R.id.artist_name)
    val songname: TextView = item1.findViewById(R.id.song_name)
    val poster: ImageView = item1.findViewById(R.id.poster)

}
