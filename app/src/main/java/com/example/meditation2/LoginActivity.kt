package com.example.meditation2

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.meditation2.data.api.RetrofitObj
import com.example.meditation2.data.model.LoginResponse
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern.compile
import javax.security.auth.callback.Callback


class LoginActivity : AppCompatActivity() {
    lateinit var email_enter: EditText
    lateinit var pass_enter: EditText
    lateinit var email_pattern: String
    lateinit var preferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        email_enter = findViewById(R.id.emailEnter)
        pass_enter = findViewById(R.id.passEnter)
        preferences = getSharedPreferences("main", MODE_PRIVATE)
        email_pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,50}" +
                "\\@" +
                "[a-zA-Z][a-zA-Z0-9]{0,8}" +
                "(" +
                "\\." +
                "[a-zA-Z][a-zA-Z\\-]{0,5}" +
                ")+"
    }

    fun EmailValid(em: String): Boolean {return compile(email_pattern).matcher(em).matches()}

    fun toRegister(view: android.view.View) {
        val perehod = Intent(this, EndCapRegisterActivity::class.java)
        startActivity(perehod)
    }

    fun toMainScreen(view: android.view.View) {
        val emailText = email_enter.text.toString()
        val passwordText = pass_enter.text.toString()
        if(emailText.isNotEmpty() && passwordText.isNotEmpty()){
            if(EmailValid(emailText)){
               val hashMap = hashMapOf<String, String>()
               hashMap.put("email", emailText)
               hashMap.put("password", passwordText)

               val retrofitic = RetrofitObj.getSmth().authorize(hashik = hashMap)
                retrofitic.enqueue(object : retrofit2.Callback<LoginResponse> {
                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        if (response.isSuccessful) {
                            val editor = preferences.edit()
                            editor.putString("avatar", response.body()?.avatar)
                            editor.putString("token", response.body()?.token)
                            editor.putString("nickName", response.body()?.nickName)
                            editor.putString("email", response.body()?.email)
                            editor.apply()
                            val main = Intent(this@LoginActivity, MainScreenActivity::class.java)
                            startActivity(main)
                        }
                        else{
                            Toast.makeText(this@LoginActivity, response.errorBody()?.string(), Toast.LENGTH_SHORT).show()
                        }
                        }

                        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                            Toast.makeText(
                                this@LoginActivity,
                                t.localizedMessage,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    })





                }else{
                val loginalert = AlertDialog.Builder(this)
                    .setTitle("Ошибка входа")
                    .setMessage("Некорректно введен E-mail")
                    .setPositiveButton("Ок", null)
                    .create()
                    .show()
                }
        }
        else{
         // Toast.makeText(this, "У Вас есть пустые поля", Toast.LENGTH_LONG).show()
         val loginalert = AlertDialog.Builder(this)
             .setTitle("Ошибка входа")
             .setMessage("У Вас есть пустые поля")
             .setPositiveButton("Ок", null)
             .create()
             .show()
        }

    }

}