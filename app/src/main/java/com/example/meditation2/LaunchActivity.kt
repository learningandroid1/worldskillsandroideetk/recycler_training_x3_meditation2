package com.example.meditation2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        val pref = getSharedPreferences("main", MODE_PRIVATE)
        val timer = object: CountDownTimer(5000,200){
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
              val token = pref.getString("token", null)
              if(token != null) {
                  val perehod = Intent(this@LaunchActivity, MainScreenActivity::class.java )
                  startActivity(perehod)
              }
              else{
              val perehod = Intent(this@LaunchActivity, OnBoardingActivity::class.java )
              startActivity(perehod)
              }
              finish()
            }

        }
        timer.start()
    }
}