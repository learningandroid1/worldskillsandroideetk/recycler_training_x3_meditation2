package com.example.meditation2.ui.profile

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.meditation2.LoginActivity
import com.example.meditation2.R

class ProfileFragment: Fragment(R.layout.fragment_profile) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val knopka: Button = view.findViewById(R.id.exit_button)
        val prefes: SharedPreferences? = activity?.getSharedPreferences("main", MODE_PRIVATE)
        knopka.setOnClickListener{
           if(prefes != null){
               val prefesedit = prefes.edit()
               prefesedit.putString("token", null)
               prefesedit.apply()
           }
        val intent = Intent(requireContext(), LoginActivity::class.java)
            activity?.startActivity(intent)
        }
    }
}