package com.example.meditation2.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.meditation2.MyFeel
import com.example.meditation2.R
import com.example.meditation2.adapters.AdapterRecHoriz
import com.example.meditation2.adapters.AdapterRecVert
import com.example.meditation2.data.api.RetrofitObj
import com.example.meditation2.data.model.FeelingsResponse
import com.example.meditation2.data.model.QuotesResponse
import com.example.meditation2.databinding.FragmentHomeBinding
import retrofit2.Call
import retrofit2.Response

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val avatar: ImageView = root.findViewById(R.id.avatar_pict)
        val hellotext: TextView = root.findViewById(R.id.hello_text)
        val prefes2: SharedPreferences? = activity?.getSharedPreferences("main", Context.MODE_PRIVATE)
        val name = prefes2?.getString("nickName", "Default")
        val recycler: RecyclerView = root.findViewById(R.id.recycler_horiz)
        val recycler2: RecyclerView = root.findViewById(R.id.recycler_vert)

        Glide.with(requireContext()).load(prefes2?.getString("avatar", "")).circleCrop().into(avatar)
        hellotext.text = "С возвращением, $name"

        val locretrofit = RetrofitObj.getSmth().quotesGet()
        locretrofit.enqueue(object: retrofit2.Callback<QuotesResponse>{
            override fun onResponse(
                call: Call<QuotesResponse>,
                response: Response<QuotesResponse>
            ) {
                if (response.isSuccessful){
                    recycler2.adapter = response.body()?.data?.let {list -> AdapterRecVert(requireContext(), list) }
                }

            }

            override fun onFailure(call: Call<QuotesResponse>, t: Throwable) {
                Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()
            }

        })

        val locretrofit2 = RetrofitObj.getSmth().feelingsGet()
        locretrofit2.enqueue(object: retrofit2.Callback<FeelingsResponse> {
            override fun onResponse(
                call: Call<FeelingsResponse>,
                response: Response<FeelingsResponse>
            ) {
                if (response.isSuccessful){
                    recycler.adapter = response.body()?.data?.let {AdapterRecHoriz(requireContext(), it)}
                }
            }

            override fun onFailure(call: Call<FeelingsResponse>, t: Throwable) {
                Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}