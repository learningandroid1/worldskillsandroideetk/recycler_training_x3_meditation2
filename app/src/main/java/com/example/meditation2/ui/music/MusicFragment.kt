package com.example.meditation2.ui.music

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.meditation2.MyMusic
import com.example.meditation2.R
import com.example.meditation2.adapters.AdapterMusic


class MusicFragment : Fragment(R.layout.fragment_music) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mpager: ViewPager2 = view.findViewById(R.id.music_pager)
        mpager.adapter = AdapterMusic(requireContext(), MyMusic().spisok3)
    }
}